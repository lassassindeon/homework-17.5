﻿#include <iostream>

class Vector
{
public:
    Vector() : x(4.3), y(6.9), z(9.2)
    {}

    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}

    double Show()
    {
        double V = x * x + y * y + z * z;

        return sqrt(V);
    }
private:

    double x = 0;
    double y = 0;
    double z = 0;
};

int main()
{
    Vector V;
    V.Show();

    std::cout << V.Show();
}